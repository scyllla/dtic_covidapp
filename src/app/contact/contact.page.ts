import { Component, OnInit } from '@angular/core';
import { CovidService } from '../api/covid.service';
import { Partenaire } from '../models/partenaire';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  data : Partenaire
  constructor(public caseService: CovidService,
    public router: Router) { this.data = new Partenaire();}

  ngOnInit() {
  }

  submitForm() {
    console.log(this.data)
    this.caseService.createItem(this.data).subscribe((response) => {
      this.router.navigate(['']);
    });
  }

}
