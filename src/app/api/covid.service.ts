import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Case } from '../models/case';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Partenaire } from '../models/partenaire';


@Injectable({
  providedIn: 'root'
})
export class CovidService {
  baseUrl:string = "https://covid.dticconsulting.com/api";
  //baseUrl:string = "http://localhost:8000/api";
  constructor(private http: HttpClient) { }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  // Get All data
  getList(): Observable<Case> {
    return this.http
      .get<Case>(this.baseUrl+'/allcas')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  getTotal(): Observable<Case> {
    return this.http
      .get<Case>(this.baseUrl+'/total')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  getListWillaye(): Observable<Case> {
    return this.http
      .get<Case>(this.baseUrl+'/cas/willaya')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  getInformation(): Observable<Case> {
    return this.http
      .get<Case>(this.baseUrl+'/informations')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  createItem(item): Observable<Partenaire> {
    console.log(item);
    return this.http
      .post<Partenaire>(this.baseUrl+'/partenaires', JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
        
      )
  }
}
