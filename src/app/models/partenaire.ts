export class Partenaire {
    id: number;
    nom: string;
    prenom: string;
    email: string;
    telephone: string;
    Ville: string;
    Commentaire: string;
    Adresse: string;
    adresseSecond: string;
}
