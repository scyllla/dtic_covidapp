import { Component, OnInit } from '@angular/core';
import { CovidService } from '../api/covid.service';


@Component({
  selector: 'app-region',
  templateUrl: './region.page.html',
  styleUrls: ['./region.page.scss'],
})
export class RegionPage implements OnInit {
  willayaData: any;

  constructor(public covidService: CovidService) {
    this.willayaData = [];
   }

  ngOnInit() {
  }
  

  ionViewWillEnter() {
    // Used ionViewWillEnter as ngOnInit is not 
    // called due to view persistence in Ionic
    //this.getAllCas();
    this.getAllWillaya();

  }

  getAllWillaya() {
    //Get saved list of students
    this.covidService.getListWillaye().subscribe(response => {
      //console.log(response);
      this.willayaData = response;
      console.log (this.willayaData);
    })
  }

}
