import { Component, OnInit } from '@angular/core';
import { CovidService } from '../api/covid.service';

@Component({
  selector: 'app-informations',
  templateUrl: './informations.page.html',
  styleUrls: ['./informations.page.scss'],
})
export class InformationsPage implements OnInit {

  informationsData: any;

  constructor(public caseService: CovidService) {
    this.informationsData = [];
  }

  ngOnInit() {
    this.getAllInformation();
  }

  ionViewWillEnter() {
    // Used ionViewWillEnter as ngOnInit is not 
    // called due to view persistence in Ionic
    this.getAllInformation();

  }

  getAllInformation() {
    //Get saved list of students
    this.caseService.getInformation().subscribe(response => {
      //console.log(response);
      this.informationsData = response;
      console.log (this.informationsData);
    })
  }

}
