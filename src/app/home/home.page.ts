import { Component, OnInit } from '@angular/core';
import { CovidService } from '../api/covid.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  casesData: any;
  totalData: any;

  constructor(public covidService: CovidService) {
    this.casesData = [];
    this.totalData = [];
   }

  ngOnInit() {
    //this.getAllCas();
    //this.getAllWillaya();
  }

  ionViewWillEnter() {
    // Used ionViewWillEnter as ngOnInit is not 
    // called due to view persistence in Ionic
    this.getAllCas();
    this.getAllWillaya();

  }

  getAllCas() {
    //Get saved list of students
    this.covidService.getList().subscribe(response => {
      //console.log(response);
      this.casesData = response;
      console.log (this.casesData);
    })
  }

  getAllWillaya() {
    //Get saved list of students
    this.covidService.getTotal().subscribe(response => {
      //console.log(response);
      this.totalData = response;
      console.log (this.totalData);
    })
  }

}
