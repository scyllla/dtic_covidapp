import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GestePage } from './geste.page';

describe('GestePage', () => {
  let component: GestePage;
  let fixture: ComponentFixture<GestePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GestePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
