import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GestePageRoutingModule } from './geste-routing.module';

import { GestePage } from './geste.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GestePageRoutingModule
  ],
  declarations: [GestePage]
})
export class GestePageModule {}
