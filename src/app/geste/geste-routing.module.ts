import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestePage } from './geste.page';

const routes: Routes = [
  {
    path: '',
    component: GestePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestePageRoutingModule {}
